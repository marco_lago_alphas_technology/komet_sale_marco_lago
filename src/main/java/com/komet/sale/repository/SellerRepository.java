package com.komet.sale.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.komet.sale.dto.SellerCommissionDto;
import com.komet.sale.model.Seller;

public interface SellerRepository extends JpaRepository<Seller, Long> {
	@Query(nativeQuery = true)
	public List<SellerCommissionDto> findCommission(@Param("id") Long id);
}
