package com.komet.sale.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.komet.sale.model.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long>{

	@Query(nativeQuery = true)
	public List<Sale> findAll();
	
	@Query(nativeQuery = true)
	public void deleteById(@Param("id") Long id);
	
	@Modifying
	@Query(nativeQuery = true)
	public void insert(@Param("idSeller") String idSeller, @Param("idProduct") String idProduct, @Param("saleDate") String saleDate, @Param("amount") String amount);
	
	@Query( nativeQuery = true)
	public void loadCsv(@Param("fileCsv")String fileCsv);
}
