-- Schemas 
CREATE SCHEMA `komet_sales` DEFAULT CHARACTER SET utf8 ;

-- System Tables
CREATE TABLE `seller` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Seller id',
  `name` varchar(50) NOT NULL COMMENT 'Seller name',
  `email` varchar(50) NOT NULL COMMENT 'Seller Email',
  `percentage` double NOT NULL COMMENT 'Commission percentage',
  `avatar` varchar(255) NOT NULL COMMENT 'Seller avatar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 
COMMENT='Vendor master table';

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Product id',
  `name` varchar(45) NOT NULL COMMENT 'Product name',
  `description` varchar(100) NOT NULL COMMENT 'Product description',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 
COMMENT='Product master';

CREATE TABLE `sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sale id',
  `id_seller` int(11) NOT NULL COMMENT 'Seller id',
  `id_product` int(11) NOT NULL COMMENT 'Product id',
  `sale_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Sale date',
  `amount` double NOT NULL COMMENT 'Sale amount',
  PRIMARY KEY (`id`),
  KEY `fk_sale_seller_idx` (`id_seller`),
  KEY `fk_sale_product_idx` (`id_product`),
  CONSTRAINT `fk_sale_product` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sale_seller` FOREIGN KEY (`id_seller`) REFERENCES `seller` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 
COMMENT='Sales movement';

-- Initialize data
INSERT INTO `komet_sales`.`seller` (`name`, `email`, `percentage`, `avatar`) VALUES ('Juan P�rez', 'juan.perez@gmail.com', '3', 'img/1.jpg');
INSERT INTO `komet_sales`.`seller` (`name`, `email`, `percentage`, `avatar`) VALUES ('Manuel Gonz�lez', 'manuel.gonzalez@gmail.com', '2.1', 'img/2.jpg');
INSERT INTO `komet_sales`.`seller` (`name`, `email`, `percentage`, `avatar`) VALUES ('Carolina M�ndez', 'carolina.mendez@gmail.com', '1.95', 'img/3.jpg');
INSERT INTO `komet_sales`.`seller` (`name`, `email`, `percentage`, `avatar`) VALUES ('Mar�a Rodr�guez', 'maria.rodriguez@gmail.com', '0.33', 'img/4.jpg');

INSERT INTO `komet_sales`.`product` (`name`, `description`) VALUES ('Producto 1', 'Producto de prueba 1');
INSERT INTO `komet_sales`.`product` (`name`, `description`) VALUES ('Product 2', 'Producto de prueba 2');
INSERT INTO `komet_sales`.`product` (`name`, `description`) VALUES ('Product 3', 'Producto de prueba 3');

INSERT INTO `komet_sales`.`sale` (`id_seller`, `id_product`, `amount`) VALUES ('1', '1', '10');
INSERT INTO `komet_sales`.`sale` (`id_seller`, `id_product`, `amount`) VALUES ('2', '1', '10');
INSERT INTO `komet_sales`.`sale` (`id_seller`, `id_product`, `amount`) VALUES ('3', '1', '10');
INSERT INTO `komet_sales`.`sale` (`id_seller`, `id_product`, `amount`) VALUES ('4', '1', '10');

