package com.komet.sale.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.komet.sale.dto.SellerCommissionDto;
import com.komet.sale.model.Seller;
import com.komet.sale.service.SellerService;

@RestController
@RequestMapping("seller")
public class SellerController {

	@Autowired
	private SellerService service;

	@GetMapping
	public List<Seller> findAll() {
		List<Seller> listSeller = service.findAll();
		return listSeller;
	}

	@GetMapping("{id}")
	public SellerCommissionDto editCustomerForm(@PathVariable long id) {
		SellerCommissionDto seller = service.getSellerCommission(id).get(0);
		return seller;
	}

}
