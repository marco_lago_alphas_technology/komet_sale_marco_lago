package com.komet.sale.service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.joda.DateTimeParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.komet.sale.model.Sale;
import com.komet.sale.repository.SaleRepository;

@Service
@Transactional
public class SaleService {

	@Autowired
	private SaleRepository repository;

	public List<Sale> findAll() {
		return repository.findAll();
	}

	public Timestamp parseTimestamp(String date) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date parsedDate = dateFormat.parse(date);
		return new java.sql.Timestamp(parsedDate.getTime());
	}

	@Async
	public boolean importCsv(String csvFile) throws NumberFormatException, ParseException {
		boolean success = false;

		try (Reader reader = Files.newBufferedReader(Paths.get(csvFile));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {
			for (CSVRecord csvRecord : csvParser) {
				repository.insert(csvRecord.get(1), csvRecord.get(2), csvRecord.get(0), csvRecord.get(3));
			}
			success = true;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return success;

	}

	public static void main(String args[]) throws NumberFormatException, ParseException {
		SaleService service = new SaleService();
		
		service.importCsv(System.getenv("KOMETSALE_HOME")+"/tmp.csv");
	}
}
