package com.komet.sale.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.komet.sale.dto.SellerCommissionDto;
import com.komet.sale.model.Seller;
import com.komet.sale.repository.SellerRepository;;

@Service
@Transactional
public class SellerService {
	
	@Autowired
	private SellerRepository repository;
	
	public List<SellerCommissionDto> getSellerCommission(Long id){
		return repository.findCommission(id);
	}
	
	public Seller getById(Long id) {
		return repository.getOne(id);
	}
	
	public List<Seller> findAll()
	{
		return repository.findAll();
	}

}
