package com.komet.sale.dto;

public class SellerCommissionDto {

	private String name;
	private String email;
	private String avatar;
	private Double percentage;
	private Double commission;

	public String getName() {
		return name;
	}
	
	public SellerCommissionDto()
	{
		
	}

	public SellerCommissionDto(String name, String email, String avatar, Double percentage, Double commission) {
		super();
		this.name = name;
		this.email = email;
		this.avatar = avatar;
		this.percentage = percentage;
		this.commission = commission;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}
}
