package com.komet.sale.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the sale database table.
 * 
 */
@Entity
@NamedNativeQueries({ 
	@NamedNativeQuery(name = "Sale.findAll", query = "SELECT id, id_seller, id_product, sale_date, amount FROM sale"),	
	@NamedNativeQuery(name = "Sale.findById", query = "SELECT id, id_seller, id_product, sale_date, amount FROM sale WHERE id = :id"),
	@NamedNativeQuery(name = "Sale.updateById", query = "UPDATE SALE SET id_seller = :idSeller, id_product = :idProduct, sale_date = :saleDate, amount = :amount WHERE id = :id"),	
	@NamedNativeQuery(name = "Sale.deleteById", query = "DELETE FROM sale WHERE id = :id"),
	@NamedNativeQuery(name = "Sale.loadCsv", query = "LOAD DATA INFILE ':fileCsv' INTO TABLE sale FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 ROWS (id_seller, id_product, @sale_date, amount) SET sale_date = STR_TO_DATE(@sale_date, '%d/%m/%Y')"),	
	@NamedNativeQuery(name = "Sale.insert", query = "INSERT INTO sale (id_seller, id_product, sale_date, amount ) VALUES (:idSeller, :idProduct, :saleDate, :amount)") 
})
public class Sale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Double amount;

	@Column(name="sale_date")
	private Timestamp saleDate;

	//bi-directional many-to-one association to Product
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_product")
	private Product product;

	//bi-directional many-to-one association to Seller
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_seller")
	private Seller seller;

	public Sale() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Timestamp getSaleDate() {
		return this.saleDate;
	}

	public void setSaleDate(Timestamp saleDate) {
		this.saleDate = saleDate;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Seller getSeller() {
		return this.seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

}