package com.komet.sale.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@NamedNativeQueries({ 
	@NamedNativeQuery(name = "Product.findAll", query = "SELECT id, name, description FROM product"),
	@NamedNativeQuery(name = "Product.findById", query = "SELECT id, name, description FROM product WHERE id = :id"),
	@NamedNativeQuery(name = "Product.updateById", query = "UPDATE product SET name = :name, description = :description WHERE id = :id"),	
	@NamedNativeQuery(name = "Product.deleteById", query = "DELETE FROM product WHERE id = :id"),
	@NamedNativeQuery(name = "Product.insert", query = "INSERT INTO product (name, description) VALUES (:name, :description)") 
})
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String description;
	private String name;
	private List<Sale> sales;

	public Product() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// bi-directional many-to-one association to Sale
	@OneToMany(mappedBy = "product")
	public List<Sale> getSales() {
		return this.sales;
	}

	public void setSales(List<Sale> sales) {
		this.sales = sales;
	}

	public Sale addSale(Sale sale) {
		getSales().add(sale);
		sale.setProduct(this);

		return sale;
	}

	public Sale removeSale(Sale sale) {
		getSales().remove(sale);
		sale.setProduct(null);

		return sale;
	}

}