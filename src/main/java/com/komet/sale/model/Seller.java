package com.komet.sale.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.komet.sale.dto.SellerCommissionDto;

import java.util.List;

/**
 * The persistent class for the seller database table.
 * 
 */
@Entity
@SqlResultSetMapping(
        name="sellerMapping",
        classes={
           @ConstructorResult(
                targetClass=SellerCommissionDto.class,
                  columns={
                     @ColumnResult(name="name"),
                     @ColumnResult(name="email"),
                     @ColumnResult(name="avatar"),
                     @ColumnResult(name="percentage"),
                     @ColumnResult(name="commission"),
                     }
           )
        }
)
@NamedNativeQueries({
		@NamedNativeQuery(name = "Seller.findAll", query = "SELECT id, name, email, percentage, avatar FROM seller"),
		@NamedNativeQuery(name = "Seller.findById", query = "SELECT id, name, email, percentage, avatar FROM seller WHERE id = :id"),
		@NamedNativeQuery(name = "Seller.findCommission", query = ""
				+ "SELECT B.name, B.email, B.avatar, B.percentage, SUM(CASE WHEN A.amount IS NULL THEN 0.0 ELSE A.amount END * B.percentage / 100) as commission "
				+ "FROM sale as A RIGHT JOIN seller as B ON A.id_seller = B.id "
				+ "WHERE B.id = :id GROUP BY B.name, B.email, B.avatar, B.percentage", resultSetMapping = "sellerMapping"),
		@NamedNativeQuery(name = "Seller.updateById", query = "UPDATE seller SET name = :name, email = :email, percentage = :percentage, avatar = :avatar WHERE id = :id"),
		@NamedNativeQuery(name = "Seller.deleteById", query = "DELETE FROM seller WHERE id = ?"),
		@NamedNativeQuery(name = "Seller.insert", query = "INSERT INTO seller (name, email, percentage, avatar) VALUES (:name, :email, :percentage, :avatar)") })
public class Seller implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String avatar;

	private String email;

	private String name;

	private double percentage;

	// bi-directional many-to-one association to Sale
	@JsonIgnore
	@OneToMany(mappedBy = "seller")
	private List<Sale> sales;

	public Seller() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPercentage() {
		return this.percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public List<Sale> getSales() {
		return this.sales;
	}

	public void setSales(List<Sale> sales) {
		this.sales = sales;
	}

	public Sale addSale(Sale sale) {
		getSales().add(sale);
		sale.setSeller(this);

		return sale;
	}

	public Sale removeSale(Sale sale) {
		getSales().remove(sale);
		sale.setSeller(null);

		return sale;
	}

}