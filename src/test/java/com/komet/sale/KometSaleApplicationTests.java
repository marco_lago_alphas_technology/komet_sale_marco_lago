package com.komet.sale;

import java.text.ParseException;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.komet.sale.dto.SellerCommissionDto;
import com.komet.sale.model.Sale;
import com.komet.sale.service.SaleService;
import com.komet.sale.service.SellerService;

@SpringBootTest
class KometSaleApplicationTests {

	@Mock
	private SaleService saleService;
	
	@Mock
	private SellerService sellerService;
	
	@Test
	void testSellerList() {
		List<Sale> list = saleService.findAll();
		Assertions.assertNotNull(list);
	}

	@Test
	void testGetSellerCommission() {
		Long id = 2L;
		List<SellerCommissionDto> sellerCommision = sellerService.getSellerCommission(id);
		Assertions.assertNotNull(sellerCommision);
	}

	@Test
	void testSalesLoad() throws NumberFormatException, ParseException {
		boolean success = saleService.importCsv(System.getenv("KOMETSALE_HOME")+"/tmp.csv");		
		Assertions.assertFalse(success);
	}

}
